/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package printer;

 

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Semaphore;
/**
 *
 * @author camelo
 */
public class Main {
	
	 private static final int MAX_PRINTS = 6;
	 private static final int JOBS = 20;
	 private static final int PACKET_SIZE = 10;
         private static final int PACKAGE_PRINTING_TIME = 50; // 5 seconds

	 public static int jobOnBufferCounter = 0;
	 public static int[] jobOnBufferId = new int[JOBS];
	 
	 public static Semaphore queue = new Semaphore(0,true);
	 public static Semaphore checkIn = new Semaphore(1,true);
	 public static Semaphore enteringBuffer = new Semaphore(0,true);
	 public static Semaphore printing = new Semaphore(0,true);
	 public static Semaphore unloading = new Semaphore(0,true);
	 
	 
	 public static void main(String args[]){
		
		Printer printer = new Printer(PACKET_SIZE,MAX_PRINTS, PACKAGE_PRINTING_TIME);
		
		
		for (int i=0; i< JOBS;i++){
		
                        //Start treads of all jobs
			Job aux = new Job(i,PACKET_SIZE,printer);
			Thread auxT= new Thread(aux);
			auxT.start();
			
		}
		
                //start printer thread
		Thread printerThread = new Thread(printer);
		 
		printerThread.start();
		
	}
}