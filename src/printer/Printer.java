/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package printer;

/**
 *
 * @author camelo
 */
public class Printer implements Runnable {

    private int maxTimes;
    private int maxJobs;
    private int packagePrintingTime;
    private boolean printing = false;
    private boolean isBufferFull = false;

    public Printer(int jobs, int times, int printingTime) {

        maxTimes = times;
        maxJobs = jobs;
        packagePrintingTime = printingTime;
    }

    public void run() {
        try {
            int toPrintIndex = maxTimes+1;

            while (maxTimes >= 1) {

                //if package has size of maxJobs means printer is ready to print
                checkBuffer();

                printPackage(toPrintIndex);
                
                //empty buffer since all the previous jobs have been printed
                Main.jobOnBufferCounter = 0;

                maxTimes--;
                printing = false;
                if(maxTimes >= 1)
                    setFullBuffer(false);

            }
            System.out.println("Todas Impressões do Dia Foram realizadas");
            System.exit(0);
        } catch (InterruptedException e) {
            System.out.println("Processamento da impressora interrompido.");
        }

    }
    public synchronized void printPackage(int toPrintIndex){
        //set printing flag to true
                printing = true;

                System.out.println("Impressora está na " + (toPrintIndex - maxTimes) + "ª impressão");
                System.out.println("Jobs sendo impressos:");

                //Print the entire package
                for (int i = 0; i < maxJobs; i++) {
                    System.out.print(Main.jobOnBufferId[i] + " ");
                }

                System.out.print("\n");

                //Simulate time to print package
                try {
                    Thread.sleep(packagePrintingTime);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
    }
    
    
    public synchronized void checkPrinter(int id)
            throws InterruptedException {

        while (printing) {
            wait();           
        }
    }

    public synchronized void addJobToBuffer(int jobId) {
        //Put job on buffer
        System.out.println("Buff counter" + Main.jobOnBufferCounter + "   ");
        Main.jobOnBufferId[Main.jobOnBufferCounter] = jobId;
        System.out.println("Job " + jobId + "foi colocado no buffer na posição " + Main.jobOnBufferCounter);
        Main.jobOnBufferCounter++;

        
        if (Main.jobOnBufferCounter == maxJobs - 1) {
            this.setFullBuffer(true);
        }

    }

    private synchronized void checkBuffer() throws InterruptedException {
        while (!isBufferFull) {
            wait();
        }
    }

    private synchronized void setFullBuffer(boolean bool) {
        this.isBufferFull = bool;        
        if (this.isBufferFull)
            notifyAll();
    }

}
