/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package printer;

/**
 *
 * @author camelo
 */
public class Job implements Runnable {

    private int jobId;
    private int bufferCapacity;
    private Printer printer;

    public Job(int id, int capacity, Printer printer) {

        jobId = id;
        bufferCapacity = capacity;
        this.printer = printer;

    }

    public void bePrinted() throws InterruptedException {
        try {
            //check printer to see if it is printing or available. 
            // If it is printing, thead will go into wait mode
            printer.checkPrinter(jobId);

            //When thread leaves check Printer it is ok to put job on Buffer, printer is not printing
            printer.addJobToBuffer(jobId);
        } catch (InterruptedException e) {
            System.out.println("Processamento do usuario interrompido.");
        }

    }

    public void run() {

        while (true) {
            try {
                Thread.sleep(1000);;
                bePrinted();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
}
